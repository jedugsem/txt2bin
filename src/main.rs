use regex::Regex;
//use std::env;
use std::fs;
use std::fs::File;
//use serde::{Deserialize,Serialize};
use jagdlib::types::*;
use std::io::prelude::*;

fn main() {
    let mut themas = vec![];
    let files = 0..5;
    let mut meta = MetaDb{
        name:"JagdDb".to_string(),
        extra_modes :vec![
            "Test".to_string(),
            "Retest".to_string(),
            "Random".to_string(),
        ],
        ..Default::default()
    };
    for i in files {
        let file = &format!(
            "/home/me/jedugsem/jagd/data/text/FG_{}_Prueffragen_mL_Februar_2021.txt",
            i + 1
        );
        let temp = get_theme(file,i);
        meta.thema_names.push(format!("Thema {}",i+1));
        meta.thema_sizes.push(temp.quest.len());
        themas.push(temp)
    }
    let db = Db {
        meta:meta.clone(),
        themas,
        ..Default::default()
    };
    println!("{:?}", meta);
    //println!("{:?}", db.themas[0].quest.len());
    let bin_data = bincode::serialize(&db).unwrap();
    let out = format!("/home/me/jedugsem/jagd/data/binary/{}", "bindata");
    let mut file = File::create(out).unwrap();
    println!("{:?}", file.write_all(&bin_data));
}

fn get_theme(filename: &str, thema: usize) -> Thema {
    //println!("Thema: {} Filename: {}", thema + 1, filename);

    let contents = fs::read_to_string(filename).unwrap();
    let lines: Vec<&str> = contents.split('\n').collect();

    let regex_char = Regex::new(r"^[a-z]|[A-Z]").unwrap();
    let regex_awn = Regex::new(r"^\s\s[a-i]\)").unwrap();

    let mut temp_text = "".to_string();
    let mut temp_quest = Question {
        thema,
        ..Default::default()
    };
    let mut temp_kat = Thema {
        name: format!("Thema {}", thema + 1),
        num: thema,
        ..Default::default()
    };
    let mut possable_beginning = 0;
    let mut begun = false;
    let mut index = 0;
    let mut awnser = false;
    let mut question = false;
    let mut awnser_i = 0;

    for i in lines {
        if i.starts_with("1. ") && !begun {
            //searches for the beginning of questions
            match thema {
                0 => {
                    possable_beginning += 1;
                    if possable_beginning == 3 {
                        begun = true;
                    }
                }
                _ => {
                    begun = true;
                }
            }
        }
        if begun {
            if question && i.starts_with(' ') {
                //wenn eine Frage gesucht wird und die nachste Zeile leer ist wird diese
                //gespeichert
                question = false;
                temp_quest.question = temp_text.split_once(". ").unwrap().1.trim().to_string();
                temp_text = "".to_string();
            }

            if regex_awn.is_match(i) {
                //wenn antwort dann schreibe
                
                if !temp_text.is_empty() {
                    temp_quest
                        .awnsers
                        .push((awnser_i, temp_text.trim()[2..].trim().to_string(), false));
                    temp_text = "".to_string();
                    awnser_i += 1;
                }
                awnser = true;
            } else if awnser && i.starts_with(' ') {
                if !temp_text.is_empty() {
                    temp_quest.awnsers.push((
                        awnser_i,
                        temp_text.trim()[2..].trim().to_string(),
                        false,
                    ));
                    temp_text = "".to_string();
                }
                awnser_i = 0;
                awnser = false;
            }
            if i.starts_with(&format!("{}. ", index + 1)) 
        {
                if index != 0 {
                    temp_kat.quest.push(temp_quest);
                }
                temp_quest = Question {
                    num: index,
                    thema,
                    ..Default::default()
                };
                index += 1;
                question = true;
            }
            if (question || awnser) && regex_char.is_match(i) {
                temp_text = format!("{}{}", temp_text, i);
            }
        }
    }

    temp_kat.quest.push(temp_quest);
    //println!("Questions{}", index);

    temp_kat
}
